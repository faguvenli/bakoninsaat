<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use App\Project;
use App\FloorPlan;
use App\ProjectPhotos;
use Storage;
use Image;
use Session;

class ProjectController extends Controller
{
    private $image_small_width = 471;
    private $image_medium_width = 800;
    private $image_large_width = 1920;
    private $image_cropped_path = 'images/projects/cropped/';
    private $image_small_path = 'images/projects/small/';
    private $image_medium_path = 'images/projects/medium/';
    private $image_large_path = 'images/projects/large/';

    protected function gorsel_yukle($name, $project_name, $image) {

        $edited_image_name = str_slug($project_name).time().rand().'.'.$image->getClientOriginalExtension();

        $location_cropped = public_path($this->image_cropped_path.$edited_image_name);
        $location_small = public_path($this->image_small_path.$edited_image_name);
        $location_medium = public_path($this->image_medium_path.$edited_image_name);
        $location_large = public_path($this->image_large_path.$edited_image_name);

        if($name == 'logo') {
          Image::make($image)->resize(230,115)->save($location_small);
        } else {
          Image::make($image)->fit(207,138, function($constraint) { $constraint->upsize(); })->save($location_cropped);
          Image::make($image)->resize($this->image_small_width,null, function($constraint) { $constraint->aspectRatio(); $constraint->upsize(); })->save($location_small);
          Image::make($image)->resize($this->image_medium_width,null, function($constraint) { $constraint->aspectRatio(); $constraint->upsize(); })->save($location_medium);
          Image::make($image)->resize($this->image_large_width,null, function($constraint) { $constraint->aspectRatio(); $constraint->upsize(); })->save($location_large);
        }
        return $edited_image_name;

    }

    public function delete_project_image($id,$name) {


      if($name == 'gorseller') {

        $project_photo = ProjectPhotos::find($id);

        $image_name = $project_photo->gorsel;

        if(file_exists($this->image_small_path.$image_name)) {
          File::delete(public_path($this->image_small_path.$image_name));
          File::delete(public_path($this->image_medium_path.$image_name));
          File::delete(public_path($this->image_large_path.$image_name));
        }
        $project_photo->delete($project_photo->id);
      }
      elseif($name == 'katplani') {
        $katplani_gorsel = FloorPlan::find($id);

        $image_name = $katplani_gorsel->gorsel;

        if(file_exists($this->image_small_path.$image_name)) {
          File::delete(public_path($this->image_small_path.$image_name));
          File::delete(public_path($this->image_medium_path.$image_name));
          File::delete(public_path($this->image_large_path.$image_name));
        }
        $katplani_gorsel->update(['gorsel'=>null]);
      }
      else {

        $project = Project::find($id);

        if($name == 'logo') { $image_name = $project->logo; }
        if($name == 'proje_gorsel') { $image_name = $project->proje_gorsel; }
        if($name == 'kat_plani_gorsel') { $image_name = $project->kat_plani_gorsel; }

        if(file_exists($this->image_small_path.$image_name)) {
          File::delete(public_path($this->image_small_path.$image_name));
          File::delete(public_path($this->image_medium_path.$image_name));
          File::delete(public_path($this->image_large_path.$image_name));
        }

        $project->update([$name=>null]);
      }



      Session::flash('success', 'Görsel Başarıyla Silindi.');
      return redirect()->back();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $projects = Project::all();
      return view('panel.projects')->with('projects', $projects);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('panel.ops.projects.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->only('durum', 'adi','baslangic_tarihi','ongorulen_bitis_tarihi','satisa_acik_daire','arsa_alani','insaat_alani','yesil_alan','toplam_daire','slogan','genel_ozellikler','adres','telefon','koordinatlar');

        $this->validate($request, array(
          'adi' => 'required',
        ));

        if($request->logo) {
          $request->validate(['logo'=>'image|mimes:jpeg,png,jpg,gif,svg|max:2048']);
          $data['logo'] = $this->gorsel_yukle('logo','logo_'.$request->adi, $request->file('logo'));
        }

        if($request->proje_gorsel) {
          $request->validate(['proje_gorsel'=>'image|mimes:jpeg,png,jpg,gif,svg|max:2048']);
          $data['proje_gorsel'] = $this->gorsel_yukle('proje_gorsel','proje_'.$request->adi, $request->file('proje_gorsel'));
        }

        if($request->kat_plani_gorsel) {
          $request->validate(['kat_plani_gorsel'=>'image|mimes:jpeg,png,jpg,gif,svg|max:2048']);
          $data['kat_plani_gorsel'] = $this->gorsel_yukle('kat_plani_gorsel','kat_plani_'.$request->adi, $request->file('kat_plani_gorsel'));
        }

        $project = Project::create($data);

        if($request->daire_tipi) {
          foreach($request->daire_tipi as $key => $daire_tipi) {
            if($daire_tipi) {
              $kat_plani = new FloorPlan();
              $kat_plani->project_id = $project->id;
              $kat_plani->daire_tipi = $request->input('daire_tipi')[$key];
              $kat_plani->metrekare = $request->input('metrekare')[$key];
              $kat_plani->detaylar = $request->input('detaylar')[$key];
              $kat_plani->gorsel = $this->gorsel_yukle('kat_plani_gorsel','katplani_'.$request->adi, $request->file('gorsel')[$key]);
              $kat_plani->save();
            }
          }
        }

        if($request->proje_gorselleri) {
          foreach($request->proje_gorselleri as $key => $proje_gorselleri) {
            if($proje_gorselleri) {
              $proje_gorseli = new ProjectPhotos();
              $proje_gorseli->project_id = $project->id;
              $proje_gorseli->gorsel = $this->gorsel_yukle('proje_gorselleri','projegorselleri_'.$request->adi, $request->file('proje_gorselleri')[$key]);
              $proje_gorseli->save();
            }
          }
        }

        Session::flash('success','Kayıt işlemi başarılı');
        return redirect()->route('projects.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $project = Project::find($id);
        return view("panel.ops.projects.update")->with('project',$project);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $data = $request->only('durum','adi','baslangic_tarihi','ongorulen_bitis_tarihi','satisa_acik_daire','arsa_alani','insaat_alani','yesil_alan','toplam_daire','slogan','genel_ozellikler','adres','telefon','koordinatlar');

      $this->validate($request, array(
        'adi' => 'required',
      ));

      if($request->logo) {
        $request->validate(['logo'=>'image|mimes:jpeg,png,jpg,gif,svg|max:2048']);
        $data['logo'] = $this->gorsel_yukle('logo','logo_'.$request->adi, $request->file('logo'));
        $this->delete_project_image($id,'logo');
      }

      if($request->proje_gorsel) {
        $request->validate(['proje_gorsel'=>'image|mimes:jpeg,png,jpg,gif,svg|max:2048']);
        $data['proje_gorsel'] = $this->gorsel_yukle('proje_gorsel','proje_'.$request->adi, $request->file('proje_gorsel'));
        $this->delete_project_image($id,'proje_gorsel');
      }

      if($request->kat_plani_gorsel) {
        $request->validate(['kat_plani_gorsel'=>'image|mimes:jpeg,png,jpg,gif,svg|max:2048']);
        $data['kat_plani_gorsel'] = $this->gorsel_yukle('kat_plani_gorsel','kat_plani_'.$request->adi, $request->file('kat_plani_gorsel'));
        $this->delete_project_image($id,'kat_plani_gorsel');
      }

      FloorPlan::where('project_id',$id)->whereNull('gorsel')->delete();

      if($request->daire_tipi) {
        foreach($request->daire_tipi as $key => $daire_tipi) {
          if($daire_tipi) {

            if(!$request->has("plan_id.$key")) {
              $kat_plani = new FloorPlan();
              $kat_plani->project_id = $id;
              $kat_plani->daire_tipi = $request->input('daire_tipi')[$key];
              $kat_plani->metrekare = $request->input('metrekare')[$key];
              $kat_plani->detaylar = $request->input('detaylar')[$key];
              if($request->file('gorsel')[$key]) {
                $kat_plani->gorsel = $this->gorsel_yukle('kat_plani_gorsel','katplani_'.$request->adi, $request->file('gorsel')[$key]);
              }
              $kat_plani->save();
            } else {

              $veri['project_id'] = $id;
              $veri['daire_tipi'] = $request->input('daire_tipi')[$key];
              $veri['metrekare'] = $request->input('metrekare')[$key];
              $veri['detaylar'] = $request->input('detaylar')[$key];
              if($request->file('gorsel')[$key]) {
                $veri['gorsel'] = $this->gorsel_yukle('kat_plani_gorsel','katplani_'.$request->adi, $request->file('gorsel')[$key]);
              }

              FloorPlan::updateOrCreate(['id'=>$request->input('plan_id')[$key]],$veri);
            }
          }
        }
      }

      if($request->proje_gorselleri) {
        foreach($request->proje_gorselleri as $key => $proje_gorselleri) {
          if($proje_gorselleri) {
            $proje_gorseli = new ProjectPhotos();
            $proje_gorseli->project_id = $id;
            $proje_gorseli->gorsel = $this->gorsel_yukle('proje_gorselleri','projegorselleri_'.$request->adi, $request->file('proje_gorselleri')[$key]);
            $proje_gorseli->save();
          }
        }
      }

      $project = Project::find($id);
      $project->update($data);

      Session::flash('success', 'Düzenleme işlemi başarılı.');

      return redirect()->route("projects.edit",['id'=>$id]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $kayit = Project::find($id);

      $this->delete_project_image($id,'logo');
      $this->delete_project_image($id,'proje_gorsel');
      $this->delete_project_image($id,'kat_plani_gorsel');

      $kayit->delete($kayit->id);

      // Redirect
      Session::flash('success', 'Silme işlemi başarılı!');
      return redirect()->route('projects.index');
    }
}

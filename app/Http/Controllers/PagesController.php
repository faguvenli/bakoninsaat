<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;
use Carbon\Carbon;

class PagesController extends Controller
{
    private function date_difference($start_date,$finish_date) {
        $date_1 = strtotime($start_date);
        $date_2 = strtotime($finish_date);
        $today = strtotime(date('Y-m-d'));

        $bitme_suresi = $date_2-$date_1;
        $gecen_zaman = $today-$date_1;

        $bitme_suresi = ($bitme_suresi/60/60/24);
        $gecen_zaman = ($gecen_zaman/60/60/24);

        $yuzde = 100/($bitme_suresi/$gecen_zaman);
        if(round($yuzde) > 100) {
          $sonuc = 100;
        } else {
          $sonuc = round($yuzde);
        }

        return $sonuc;

    }

    public function getHome() {
      return view('pages.home');
    }

    public function getProjectDetail($id) {
      $project = Project::find($id);
      $tarih = $this->date_difference($project->baslangic_tarihi,$project->ongorulen_bitis_tarihi);
      return view('pages.project_detail')->with('project',$project)->with('tarih',$tarih);
    }

    public function getTamamlananProjeler() {
      $projects = Project::where('durum',0)->get();
      $durum = 'Tamamlanan';
      return view('pages.projects')->with('projects',$projects)->with('durum',$durum);
    }

    public function getDevamEdenProjeler() {
      $projects = Project::where('durum',1)->get();
      $durum = 'Devam Eden';
      return view('pages.projects')->with('projects',$projects)->with('durum',$durum);
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $guarded = ['id'];

    public function kat_planlari() {
      return $this->hasMany('App\FloorPlan');
    }

    public function gorseller() {
      return $this->hasMany('App\ProjectPhotos');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectPhotos extends Model
{
    protected $guarded = ['id'];

    public function proje() {
      return $this->belongsTo('App\Project');
    }
}

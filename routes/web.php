<?php

Auth::routes();

Route::get('/', 'PagesController@getHome');
Route::get('/project_detail/{id}/{adi}', 'PagesController@getProjectDetail');
Route::get('/tamamlanan_projeler', 'PagesController@getTamamlananProjeler');
Route::get('/devam_eden_projeler', 'PagesController@getDevamEdenProjeler');

Route::middleware(['auth'])->group(function() {
  // Anasayfa
  Route::get('/panel','HomeController@index');

  // Kullanıcılar
  Route::resource('/panel/users','UserController');
  Route::get('/panel/profile','UserController@profile')->name('profile');
  Route::post('/panel/profile/update/{user}','UserController@profile_update')->name('profile.update');

  // Projeler
  Route::resource('/panel/projects','ProjectController');
  Route::get('/panel/delete_project_image/{id}/{name}','ProjectController@delete_project_image');
});

@extends('layouts.master')

@section('content')

<div class="container-fluid d-none d-md-block">
  <div class="row">
    <div class="col p-0">
      <div class="master-slider ms-skin-default" id="masterslider">
						<!-- new slide -->

						<div class="ms-slide">
              <!-- slide image layer -->
              <img src="{{ asset('assets/masterslider/blank.gif') }}" data-src="{{ asset('images/slider2.jpg') }}"/>
							<!-- end of slide image layer -->
              <!-- slide text layer -->
              <div class="ms-layer ms-caption effect_holder"
                data-type          = "text"
                data-origin = "tl"
                data-offset-y = "300"
                data-offset-x = "180"
                data-effect        = "fade(90)"
                data-duration      = "800"
                data-ease          = "easeOutQuart"
                data-resize        = "true"
                data-fixed         = "false"
                data-widthlimit    = "500">
                  <div class="container-fluid home" style="position:relative">
                     <h1><b>Siz de vakit kaybetmeden</b></h1>
                     <h1 class="ml-0 ml-6"><b>bu ayrıcalıklı projede yerinizi alın...</b></h1>
                  </div>

              </div>

              <div class="ms-layer ms-caption effect_holder"
                data-type          = "text"
                data-origin = "bl"
                data-offset-y = "320"
                data-offset-x = "180"
                data-effect        = "fade(90)"
                data-duration      = "800"
                data-ease          = "easeOutQuart"
                data-resize        = "true"
                data-fixed         = "false"
                data-widthlimit    = "500">
                  <div class="container-fluid">
                     <div class="row ml-6 mt-3">
                       <div class="col-auto col-auto mr-3 mb-1">
                         <div class="red_box_holder row">
                           <div class="red_box col-auto">3+1</div>
                           <div class="col my-auto"><h5>180 m<sup>2</sup> Lüks Daireler</h5></div>
                         </div>
                       </div>
                       <div class="col-auto">
                         <div class="red_box_holder row">
                           <div class="red_box col-auto">&nbsp;</div>
                           <div class="col my-auto"><h5>Öngörülen bitiş tarihi<br>30 Temmuz 2019</h5></div>
                         </div>
                       </div>
                     </div>

                  </div>

              </div>

              <div class="ms-layer ms-caption effect_holder"
                data-type          = "text"
                data-origin = "tl"
                data-offset-y = "620"
                data-offset-x = "40"
                data-effect        = "fade(90)"
                data-duration      = "800"
                data-ease          = "easeOutQuart"
                data-resize        = "false"
                data-fixed         = "false"
                data-widthlimit    = "500">
                  <div class="container">
                    <div class="row">
                      <div class="col-2 col-xl-2">
                         <img src="{{ asset('images/bakon_derebahcem.jpg') }}" class="img-fluid w-100">
                      </div>
                      <div class="col-2 my-auto proje_text">
                         Samsun'un merkezi 56'larda lüks daireler. Siz de vakit kaybetmeden bu ayrıcalıklı projede yerinizi alın...<br>
                         <button class="btn btn-danger mt-1 d-none d-xl-block">Proje detayı için tıklayınız</button>
                      </div>
                    </div>
                  </div>

              </div>

              <!-- end of text layer -->

						</div>
						<!-- end slide -->





					</div> <!-- master slider end -->
    </div>
  </div>
</div>
<div class="container mt-0 mt-md-1">
  <div class="row row-eq-height">
    <div class="col-12 col-lg-6 mb-3">
      <h2 class="mb-1 d-none d-md-block"><strong>Neler Yaptık</strong> (Video)</h2>
      <div class="video">
        <img src="{{ asset('images/video.jpg') }}" class="img-fluid w-100">
      </div>
    </div>
    <div class="col-12 col-lg-6">
      <h2 class="mb-1"><strong>Haberler</strong>&amp;Duyurular</h2>
      <div class="row">
        <div class="col-12 col-md-6 mb-3">
          <div class="card">
            <img class="card-img-top" src="{{ asset('images/haber1.jpg') }}" alt="Haber Başlık">
            <div class="card-body">
              <span>18.10.2018</span>
              <h5 class="card-title">Bakon Ellialtılar projemiz başladı..</h5>
              <p class="card-text">Şehrin kalbinde sınırlı sayıda özel tasarım daireler.</p>
              <a href="#" class="arrow_link"><strong>Detaylı bilgi</strong> için tıklayın</a>
            </div>
          </div>
        </div>
        <div class="col-12 col-md-6">
          <div class="card">
            <img class="card-img-top" src="{{ asset('images/haber2.jpg') }}" alt="Haber Başlık">
            <div class="card-body">
              <span>18.10.2018</span>
              <h5 class="card-title">Bakon Ellialtılar projemiz başladı..</h5>
              <p class="card-text">Şehrin kalbinde sınırlı sayıda özel tasarım daireler.</p>
              <a href="#" class="arrow_link"><strong>Detaylı bilgi</strong> için tıklayın</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row mb-2">
    <div class="col-12 col-md-4">
      <div class="row no-gutters">
          <div class="col-auto col-md-12 col-xl-auto">
              <img src="{{ asset('images/icon_1.jpg') }}" class="img-fluid" alt="">
          </div>
          <div class="col">
              <div class="card-block pt-5 pt-md-0 pt-xl-3 pt-xxl-4 pt-xxxl-5">
                  <h5 class="card-title"><b>Zamanında</b> Teslim Projeler</h5>
                  <p class="card-text">Projelerimizi belirttiğimiz bitiş tarihinde iskanı alınmış olarak teslim ediyoruz.</p>
              </div>
          </div>
      </div>
    </div>
    <div class="col-12 col-md-4">
      <div class="row no-gutters">
          <div class="col-auto col-md-12 col-xl-auto">
              <img src="{{ asset('images/icon_2.jpg') }}" class="img-fluid" alt="">
          </div>
          <div class="col">
              <div class="card-block pt-5 pt-md-0 pt-xl-3 pt-xxl-4 pt-xxxl-5">
                  <h5 class="card-title"><b>2 Yıl Malzeme</b> Garantisi</h5>
                  <p class="card-text">Tüm projelerimiz 2 yıl boyunca malzeme kaynaklı sorunlara karşı garantilidir.</p>
              </div>
          </div>
      </div>
    </div>
    <div class="col-12 col-md-4">
      <div class="row no-gutters">
          <div class="col-auto col-md-12 col-xl-auto">
              <img src="{{ asset('images/icon_3.jpg') }}" class="img-fluid" alt="">
          </div>
          <div class="col">
              <div class="card-block pt-5 pt-md-0 pt-xl-3 pt-xxl-4 pt-xxxl-5">
                  <h5 class="card-title"><b>5 Yıl İşçilik</b> Garantisi</h5>
                  <p class="card-text">Tüm projelerimiz 5 yıl boyunca işçilik kaynaklı sorunlara karşı garantilidir.</p>
              </div>
          </div>
      </div>
    </div>
  </div>
  <hr>
  <div class="row mt-3">
    <div class="col-12 text-center">
      <h1>Devam Eden <strong>Projelerimiz</strong></h1>
      <p>Bakon İnşaat halen Samsun'da büyük çapta konut projelerini yürütmektedir.</p>
    </div>
    <div class="owl-carousel mt-2">
      <div>
        <div><img src="{{ asset('images/proje1.jpg') }}" class="img-fluid"></div>
        <div class="mt-2">
          <h4>Bakon Derebahçem</h4>
          <p>Şehrin kalbinde sınırlı sayıda özel tasarım daireler</p>
          <a href="#" class="arrow_link"><strong>Detaylı bilgi</strong> için tıklayın</a>
        </div>
      </div>
      <div>
        <div><img src="{{ asset('images/proje1.jpg') }}" class="img-fluid"></div>
        <div class="mt-2">
          <h4>Bakon Derebahçem</h4>
          <p>Şehrin kalbinde sınırlı sayıda özel tasarım daireler</p>
          <a href="#" class="arrow_link"><strong>Detaylı bilgi</strong> için tıklayın</a>
        </div>
      </div>
      <div>
        <div><img src="{{ asset('images/proje1.jpg') }}" class="img-fluid"></div>
        <div class="mt-2">
          <h4>Bakon Derebahçem</h4>
          <p>Şehrin kalbinde sınırlı sayıda özel tasarım daireler</p>
          <a href="#" class="arrow_link"><strong>Detaylı bilgi</strong> için tıklayın</a>
        </div>
      </div>
      <div>
        <div><img src="{{ asset('images/proje1.jpg') }}" class="img-fluid"></div>
        <div class="mt-2">
          <h4>Bakon Derebahçem</h4>
          <p>Şehrin kalbinde sınırlı sayıda özel tasarım daireler</p>
          <a href="#" class="arrow_link"><strong>Detaylı bilgi</strong> için tıklayın</a>
        </div>
      </div>
    </div>
    <div class="back_line text-center w-100 mt-2">
      <button class="btn btn-danger">Tüm Projelerimiz</button>
    </div>
  </div>
  <div class="row mt-3">
    <div class="col-12 text-center">
      <h1>Tamamlanan <strong>Projelerimiz</strong></h1>
      <p>Bakon İnşaat halen Samsun'da büyük çapta konut projelerini yürütmektedir.</p>
    </div>
    <div class="owl-carousel mt-2">
      <div>
        <div><img src="{{ asset('images/proje1.jpg') }}" class="img-fluid"></div>
        <div class="mt-2">
          <h4>Bakon Derebahçem</h4>
          <p>Şehrin kalbinde sınırlı sayıda özel tasarım daireler</p>
          <a href="#" class="arrow_link"><strong>Detaylı bilgi</strong> için tıklayın</a>
        </div>
      </div>
      <div>
        <div><img src="{{ asset('images/proje1.jpg') }}" class="img-fluid"></div>
        <div class="mt-2">
          <h4>Bakon Derebahçem</h4>
          <p>Şehrin kalbinde sınırlı sayıda özel tasarım daireler</p>
          <a href="#" class="arrow_link"><strong>Detaylı bilgi</strong> için tıklayın</a>
        </div>
      </div>
      <div>
        <div><img src="{{ asset('images/proje1.jpg') }}" class="img-fluid"></div>
        <div class="mt-2">
          <h4>Bakon Derebahçem</h4>
          <p>Şehrin kalbinde sınırlı sayıda özel tasarım daireler</p>
          <a href="#" class="arrow_link"><strong>Detaylı bilgi</strong> için tıklayın</a>
        </div>
      </div>
    </div>
    <div class="back_line text-center w-100 mt-2">
      <button class="btn btn-danger">Tüm Projelerimiz</button>
    </div>
  </div>
  <div class="row mt-5">
    <div class="col-12 col-lg-6 mb-3">
      <h2>Yaşama <b>değer katan projeler...</b></h2>
      <p>Bakon İnşaat ( Bafra Konut İnşaat ) 1982 yılında Bafra’da kurulmuştur. 1982 – 1989 yılları arasında birçok konut ve işyeri projesini hayata geçirmiştir.</p>
      <p>1989'dan günümüze kadar inşaat sektöründe kendine yer edinen Bakon İnşaat; Samsun öncelikli olmak üzere Antalya, Ordu ve Kıbrıs'da özel konut inşaatları, fabrika inşaatları, alışveriş merkezi inşaatı, konut siteleri ve villa siteleri gibi birçok projeye başarıyla imza atmıştır. Bakon İnşaat halen Samsun’da büyük çapta konut projelerini yürütmektedir. Bakon İnşaat’ın temel sermayesi güven ve dürüstlüktür. Bunların devamlılığı esastır.</p>
      <p>Ayni zamanda üstlenilen işlerin zamanında ve söz verildiği gibi teslim edilmesi de vazgeçilmezdir.</p>
      <p>Kalite çıtasını her zaman en üst seviyede tutan Bakon İnşaat, dengeli büyüme sağlayabilmek için tüm yatırımlarını kendi özkaynaklarını kullanarak gerçekleştirmektedir.</p>
      <button class="btn btn-danger">Detaylı Bilgi</button>
    </div>
    <div class="col-12 col-lg-6">
      <img src="{{ asset('images/gorsel_1.jpg') }}" class="img-fluid">
    </div>
  </div>
</div>


@endsection

@section('custom_scripts')
<script>
  $(document).ready(function() {

    $(".owl-carousel").owlCarousel({
      margin:30,
      stagePadding:15
    });

    var slider = new MasterSlider();
			slider.setup( 'masterslider', {
				width: 1920,
				height: 850,
        autoHeight: true,
				space: 5,
				layout: "fullwidth",
				autoplay: true,
				centerControls:false,
        layersMode: "full",
				loop: true,
				view:'basic'
					// more slider options goes here...
					// check slider options section in documentation for more options.
			} );
			// adds Arrows navigation control to the slider.
			slider.control( 'arrows' );
  })
</script>
@endsection

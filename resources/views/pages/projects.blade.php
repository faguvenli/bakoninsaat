@extends('layouts.master')

@section('content')

<div class="container-fluid d-none d-md-block">
  <div class="row">
    <div class="col p-0">
      <img src="http://www.placehold.it/2550x250" class="img-fluid w-100" style="height:250px;">
    </div>
  </div>
</div>

<div class="container">
  <div class="row mt-5">
    <div class="col-12 text-center">
      <h1>{{ $durum }} <strong>Projelerimiz</strong></h1>
      <p>Bakon İnşaat halen Samsun'da büyük çapta konut projelerini yürütmektedir.</p>
    </div>
      @foreach($projects as $project)
      <div class="col-3 mt-3">
        <div class="card">
          <img class="card-img-top" src="{{ asset('images/projects/small/'.$project->gorseller->first()->gorsel) }}" title="{{ $project->adi }}" alt="{{ $project->adi }}">
          <div class="card-body">
            <span>{{ \Carbon\Carbon::parse($project->baslangic_tarihi)->format('d.m.Y') }}</span>
            <h5 class="card-title">{{ $project->adi }}</h5>
            <p class="card-text">{{ $project->slogan }}</p>
            <a href="{{ url('project_detail/'.$project->id.'/'.str_slug($project->adi))}}" class="arrow_link"><strong>Detaylı bilgi</strong> için tıklayın</a>
          </div>
        </div>
      </div>
      @endforeach
  </div>
</div>

@endsection

@section('custom_scripts')
<script>
  $(document).ready(function() {


  })
</script>
@endsection

@extends('layouts.master')

@section('content')

<div class="container-fluid d-none d-md-block">
  <div class="row">
    <div class="col p-0">
      <div class="master-slider ms-skin-default" id="masterslider">
						<!-- new slide -->

						<div class="ms-slide">
              <!-- slide image layer -->
              <img src="{{ asset('assets/masterslider/blank.gif') }}" data-src="{{ asset('images/projects/large/'.$project->proje_gorsel) }}"/>
							<!-- end of slide image layer -->
              <!-- slide text layer -->
              <div class="ms-layer ms-caption effect_holder"
                data-type          = "text"
                data-origin = "tl"
                data-offset-y = "210"
                data-offset-x = "180"
                data-effect        = "fade(90)"
                data-duration      = "800"
                data-ease          = "easeOutQuart"
                data-resize        = "true"
                data-fixed         = "false"
                data-widthlimit    = "500">
                  <div class="container-fluid home" style="position:relative">
                     <h1><b>Siz de vakit kaybetmeden</b></h1>
                     <h1 class="ml-0 ml-6"><b>bu ayrıcalıklı projede yerinizi alın...</b></h1>
                  </div>

              </div>

              <div class="ms-layer ms-caption"
                data-type          = "text"
                data-origin = "bl"
                data-offset-y = "320"
                data-offset-x = "180"
                data-effect        = "fade(90)"
                data-duration      = "800"
                data-ease          = "easeOutQuart"
                data-resize        = "false"
                data-fixed         = "false"
                data-widthlimit    = "500">
                  <div class="container-fluid">
                     <div class="row ml-6 mt-3">
                       <div class="col-auto mr-3 mb-1">
                         <div class="row">
                           <div class="col" style="position:relative;">
                             <div style="width:60px;">
                               <input type="text" class="dial" value="{{ $tarih }}">
                             </div>
                           </div>
                           <div class="col-auto my-auto">
                             <div>
                               Öngörülen bitiş tarihi<br><b>{{ \Carbon\Carbon::parse($project->ongorulen_bitis_tarihi)->format('d.m.Y') }}</b>
                             </div>
                           </div>
                         </div>
                       </div>
                       <div class="col-12">
                         <div class="row">
                           <div class="col-auto" style="position:relative;">
                             <div style="width:60px;">
                               <input type="text" class="dial2" value="{{ $project->satisa_acik_daire }}">
                             </div>
                           </div>
                           <div class="col-auto my-auto">
                             <div>
                               Satışa Açık Daire Sayısı
                             </div>
                           </div>
                         </div>
                       </div>
                     </div>

                  </div>

              </div>

              <div class="ms-layer ms-caption effect_holder"
                data-type          = "text"
                data-origin = "tl"
                data-offset-y = "620"
                data-offset-x = "40"
                data-effect        = "fade(90)"
                data-duration      = "800"
                data-ease          = "easeOutQuart"
                data-resize        = "false"
                data-fixed         = "false"
                data-widthlimit    = "500">
                  <div class="container">
                    <div class="row">
                      <div class="col-2 d-none d-xxl-block">
                         <img style="max-height:215px;" src="{{ asset('images/projects/small/'.$project->logo) }}" class="img-fluid w-100">
                      </div>
                      <div class="col-6 col-xxl-3 my-auto proje_text">
                         <div class="mb-1">Rakamlarla Projemiz</div>
                         <div class="row">
                           <div class="col-auto">
                             <div class="brown_box_holder2 mb-1">
                               <div class="brown_box col-auto">{{ $project->arsa_alani }}</div>
                               <div class="col my-auto p-0 text-center"><h5>Arsa<br>Alanı</h5></div>
                             </div>
                           </div>
                           <div class="col-auto">
                             <div class="brown_box_holder2 mb-1">
                               <div class="brown_box col-auto">{{ $project->insaat_alani }}</div>
                               <div class="col my-auto p-0 text-center"><h5>İnşaat<br>Alanı</h5></div>
                             </div>
                           </div>
                           <div class="col-auto">
                             <div class="brown_box_holder2 mb-1">
                               <div class="brown_box col-auto">{{ $project->yesil_alan }}</div>
                               <div class="col my-auto p-0 text-center"><h5>Yeşil<br>Alan</h5></div>
                             </div>
                           </div>
                           <div class="col-auto">
                             <div class="brown_box_holder2 mb-1">
                               <div class="brown_box col-auto">{{ $project->toplam_daire }}</div>
                               <div class="col my-auto p-0 text-center"><h5>Toplam<br>Daire</h5></div>
                             </div>
                           </div>
                         </div>
                      </div>
                    </div>
                  </div>

              </div>

              <!-- end of text layer -->

						</div>
						<!-- end slide -->

					</div> <!-- master slider end -->
    </div>
  </div>
</div>
<div class="container">
  <div class="row">
    <div class="col-12">
      <h2 class="mb-1"><strong>Kat Planları</strong></h2>
    </div>
  </div>

  <div class="row">
    @if($project->kat_planlari)
    @foreach($project->kat_planlari as $katplani)
    <div class="col-12 col-lg-6 mb-3">
      <div class="row">

        <div class="col-12 col-md mb-3">
          <div class="kat_planlari">
            <a class="fancybox" href="{{ asset('images/projects/large/'.$katplani->gorsel) }}"><img src="{{ asset('images/projects/small/'.$katplani->gorsel) }}" class="img-fluid"></a>
          </div>
        </div>
        <div class="col-12 col-md-4">
          <div class="col">
            <div class="red_box_holder2 row mb-1">
              <div class="red_box col-auto">{{ $katplani->daire_tipi }}</div>
              <div class="col my-auto"><h5>{{ $katplani->metrekare }}</h5></div>
            </div>
            <div class="row kat_plani_detay_tablo">
              {!! $katplani->detaylar !!}
            </div>
          </div>
        </div>
      </div>
    </div>
    @endforeach
    @endif

  </div>
  <hr class="mb-4">
  <div class="row">
    <div class="col-12 text-center">
      <h1><strong>Proje Görselleri</strong></h1>
      <p>Bakon İnşaat halen Samsun'da büyük çapta konut projelerini yürütmektedir.</p>
    </div>
    <div class="owl-carousel owl-theme mt-2">

          @if($project->gorseller)
          @foreach($project->gorseller as $gorsel)
            <div><a class="fancybox" rel="gorseller" href="{{ asset('images/projects/large/'.$gorsel->gorsel) }}" title="{{ $project->adi }}"><img src="{{ asset('images/projects/cropped/'.$gorsel->gorsel) }}" class="img-fluid"></a></div>
          @endforeach
          @endif
    </div>

  </div>

  <hr class="my-4">
  <div class="row">
    <div class="col-12 text-center mb-3">
      <h1><strong>Genel Özellikler</strong></h1>
      <p>{{ $project->slogan }}</p>
    </div>
    @if($project->kat_plani_gorsel)
    <div class="col-12 col-xl mb-3 proje_genel_ozellikler">
      {!! $project->genel_ozellikler !!}
    </div>
    <div class="col-12 col-xl-auto">
      <div class="kat_planlari">
        <a class="fancybox" rel="gorseller" href="{{ asset('images/projects/large/'.$project->kat_plani_gorsel) }}" title="{{ $project->adi }}"><img src="{{ asset('images/projects/medium/'.$project->kat_plani_gorsel) }}" class="img-fluid"></a>
      </div>
    </div>
    @else
    <div class="col-12 proje_genel_ozellikler">
      {!! $project->genel_ozellikler !!}
    </div>
    @endif
    <div class="col-12 mt-3">***TÜM İÇERİK BİLGİ AMAÇLI VE TÜM GÖRSELLER TEMSİLİ OLUP BAKON İNŞAAT GEREK GÖRDÜĞÜNDE HABER VERMEKSİZİN İÇERİKTE VE GÖRSELLERDE DEĞİŞİKLİK YAPABİLİR.</div>
  </div>

  <div class="row row-eq-height mt-3">
    <div class="col-6">
      <h2 class="my-3">İletişim Bilgileri</h2>
      <div style="border:5px solid #EFEFEF; overflow:auto; padding:15px;">
        <div>
          <div>
            <p>Adres: {{ $project->adres }}</p>
            <p>Telefon: {{ $project->telefon }}</p>
          </div>

          <div class="mt-3"><h2>Mesaj Gönder</h2></div>

          {!! Form::open(['url'=>'/']) !!}
            {!! Form::label('ad_soyad', 'Ad Soyad') !!}
            {!! Form::text('ad_soyad',null,['class'=>'form-control']) !!}

            {!! Form::label('eposta', 'E-posta') !!}
            {!! Form::text('eposta',null,['class'=>'form-control']) !!}

            {!! Form::label('telefon', 'Telefon') !!}
            {!! Form::text('telefon',null,['class'=>'form-control']) !!}

            {!! Form::label('mesaj', 'Mesaj') !!}
            {!! Form::text('mesaj',null,['class'=>'form-control']) !!}

            {!! Form::submit('Gönder',['class'=>'btn btn-danger float-right mt-3']) !!}
          {!! Form::close() !!}
        </div>
      </div>
    </div>
    <div class="col-6">
      <h2 class="my-3">Ulaşım Bilgileri</h2>
      <div id="map"></div>
    </div>
  </div>
</div>


@endsection

@section('custom_scripts')
<script>
  $(document).ready(function() {

    $(".owl-carousel").owlCarousel({
      items:6,
      margin:30,
      stagePadding:15
    });

    var slider = new MasterSlider();
			slider.setup( 'masterslider', {
				width: 1920,
				height: 850,
        autoHeight: true,
				space: 5,
				layout: "fullwidth",
				autoplay: true,
				centerControls:false,
        layersMode: "full",
				loop: true,
				view:'basic'
					// more slider options goes here...
					// check slider options section in documentation for more options.
			} );
			// adds Arrows navigation control to the slider.
			slider.control( 'arrows' );

      $(".dial").knob({
        'readOnly': true,
        'width':'100px',
        'fgColor':'#ff5454',
        'thickness': '0.15',
      });

      $(".dial2").knob({
        'readOnly': true,
        'width':'100px',
        'fgColor':'#e4e4e4',
        'bgColor':'#e4e4e4',
        'thickness': '0.15',
        'inputColor': '#ff5454'
      });
  })
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAgfalBHhT1ChxYdmsdlggWRCwqTgBrftM&callback=initMap" async defer></script>
<script>
    function initMap() {
      <?php
      $koordinatlar = explode(",",$project->koordinatlar);

      if(count($koordinatlar)!=2) {
        $koordinatlar[0] = 1;
        $koordinatlar[1] = 1;
      }
      ?>
      var orta = {lat: {{ $koordinatlar[0] }}, lng: {{ $koordinatlar[1] }} };
      var map = new google.maps.Map(document.getElementById('map'), {
        zoom:14,
        scrollwheel: false,
        center: orta,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      });

      /* ------------------------------MARKER 1----------------------------------------------- */

      var marker = new google.maps.Marker({
        position: {lat: {{ $koordinatlar[0] }}, lng: {{ $koordinatlar[1] }} },
        map: map
      });

      /* ----------------------------------------------------------------------------- */

    }
  $(document).ready(function() {

    $("#map").addClass("scrolloff");
				$("#map").addClass("scrolloff");
				$(".mapContainer").on("mousedown",function(){
					$("#map").removeClass("scrolloff");
				});
				$("#map").mouseleave(function(){
					$("#map").addClass("scrolloff")
				});

  })
</script>
@endsection

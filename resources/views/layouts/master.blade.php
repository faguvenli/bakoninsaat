<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Caretta') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/pages/app.js') }}"></script>
    <script src="{{ asset('assets/masterslider/masterslider.min.js') }}"></script>


    <!-- Styles -->
    <link href="{{ asset('css/pages/app.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('assets/masterslider/style/masterslider.css') }}"/>
	  <link rel="stylesheet" href="{{ asset('assets/masterslider/skins/default/style.css') }}"/> <!-- MasterSlider default skin -->
</head>
<body>
    <div class="container">
      <div class="row">
        <div class="col">
          <nav class="navbar navbar-expand-lg navbar-light">
            <a class="navbar-brand" href="/"><img src="{{ asset('images/bakon_insaat_logo.png') }}"></a>
            <div class="logo_base"></div>
            <button class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav mr-auto">
                <li class="nav-item active"><a class="nav-link" href="/">Anasayfa <span class="sr-only">(current)</span></a></li>
                <li class="nav-item"><a class="nav-link" href="#">Kurumsal</a></li>
                <li class="nav-item dropdown">
                  <a class="dropdown-toggle nav-link" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Projeler</a>

                  <ul class="dropdown-menu first" aria-labelledby="navbarDropdown">


                    <li>
                      <div class="percentage_holder">
                        <figure class="chart-one animate">
                        	<svg role="img" xmlns="http://www.w3.org/2000/svg">
                        		<circle class="circle-background"/>
                        		<circle class="circle-foreground"/>
                        	</svg>
                        	<figcaption>%...</figcaption>
                        </figure>
                      </div>
                      <div class="ml-2">
                        <a class="dropdown-item" href="{{ url('devam_eden_projeler') }}"><strong>Devam Eden</strong> Projeler</a>
                      </div>
                    </li>
                    <div class="dropdown-divider"></div>
                    <li>
                      <div class="percentage_holder">
                        <figure class="chart-two animate">
                        	<svg role="img" xmlns="http://www.w3.org/2000/svg">
                        		<circle class="circle-background"/>
                        		<circle class="circle-foreground"/>
                        	</svg>
                        	<figcaption>%100</figcaption>
                        </figure>
                      </div>
                      <div class="ml-2">
                        <a class="dropdown-item" href="{{ url('tamamlanan_projeler') }}"><strong>Tamamlanan</strong> Projeler</a>
                      </div>
                    </li>

                  </ul>

                </li>
                <li class="nav-item"><a class="nav-link" href="#">Farkımız</a></li>
                <li class="nav-item"><a class="nav-link" href="#">İletişim</a></li>
              </ul>

            </div>
          </nav>
        </div>
      </div>
    </div>
    @include('partials._messages')
    @yield('content')

    <div class="footer container-fluid" style="background-color:#F2F2F2;">
      <div class="row">
        <div class="col">
          <div class="container">
            <div class="row">
              <div class="col footer_logo_base">
                <div><img src="{{ asset('images/bakon_insaat_footer_logo.png') }}"></div>
                <div class="sub_menu">
                  <ul>
                    <li><a href="/">Anasayfa</a></li>
                    <li><a href="#">Kurumsal</a></li>
                    <li><a href="#">Devam Eden Projeler</a></li>
                    <li><a href="#">Tamamlanan Projeler</a></li>
                    <li><a href="#">Farkımız</a></li>
                    <li><a href="#">İletişim</a></li>
                  </ul>
                </div>
                <div class="footer-text">&copy; 1982-2018 Tüm hakları saklıdır | Bakon İnşaat bir Bafra Group kuruluşudur.<br><strong>Bakon İnşaat</strong> gerek gördüğünde haber vermeksizin tüm içerikte değişiklik yapabilir.</div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <a href="http://www.deltaajans.xyz" target="_blank" class="deltalogo float-right mt-3"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAaCAYAAACtv5zzAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAeFJREFUeNqsVjFPwkAUPhpi4mCoibslTk7UX0D9BWribllc7eJiogGMDjrI7EKZZWBwchF2B0hcTdgdhPgD8HvkHSnlrtyZXvK4ll6/r+/d9947MZvNRJ7WaN55yfsC/fx3NG/vA0xkPsyDVfhR7eb6KqaLoiWgi+mY7ShjaQibExh5wF9KL51ZfE8ZXoydrBW/53s+LGL3XcsIkpfCyQCnL+4TMMUTRi+UYQ3Y1IAg1IYI4C1MF3x7uPX81dfsBa0rZZBsrxAAPE7F+gAEw4xNj9hURCdLBApwAfCCgQg8Vk019aixIFCBU6xB4FrIOEyFbeAweKSR4NBGNpxclHQj/stzAE4afxI5DdI+Z3cHtkuZ3BM5D5BMSKYI2cSxDYPN2Nx/Gzqs52ne4I/dgPajXYRKJtgHInnPCdhnyVYWpYIztaZY71uCR1xeZNkeFBMJFcMTumwn3ikZAges/0pa5qpSEaZItKUCwJTB9YwyXltpOAlPZEa6mjhHBv2hp2041As4R2KQ1gEqK2ioqDmqMbo87fvafsBh8b93Nj5Ycj8cuqrhnrfW9mSS8JYQr6IbfCa6lMnGT2WFsDpVJMK0rul3EJ7QmkBBJo8scpbelUEwnl/lffB6eKkuHbz+BBgAXoNEoJ8FW0cAAAAASUVORK5CYII="><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAaCAMAAACaYWzBAAAAM1BMVEUAAACChIaChIaChIaChIaChIaChIaChIaChIaChIaChIaChIaChIaChIaChIaChIaChIZvePKWAAAAEHRSTlMAYPDAEKAggEDQMLBwkE7gMr6QhgAAAK1JREFUKM91Uu0WhSAIA8T8yIr3f9qbJnK82f654RgcYIVtwcWwJxGcScZdHrip1ovBzHK5yw1FeaRw9wykQur8IRKfLipx46t77gH6r1N5sSFcfQblCQxILXB5J9+SXBBtJAN7AVJhxgFuLSAwrYTbCuJbyKnFx/+4pcVVxbac6vMAVXIfoS8fx6yxmXvpYG13BWB0okg26zmKzUndafDEH8fgB2eOwZHewvrgfmxjDcZhFASJAAAAAElFTkSuQmCC"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFMAAAAMCAYAAAAeaoEiAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAtlJREFUeNrcV9Ft2zAQZQP/RxvEG1gblBtUG4QblN8FEisp2t9qBI6gTmB7A3UDZQNlApcXPBovV5Ju6xYFSoCQbfEdj+/e3dFvHh4/OWPM2rwec5zT/d2HKf3w+PGzjQ9ryiPgvdiaIzYAJ9+dqY8Q189xrY+fG8aXRlzbx8cQ1y2VNbK31bawT2Bs/M3pM6t3e/go/nXExZRsrXDQtwVnDgLEpgLeVs63J1sHkGtA7vYMmfu4lzy/0N5jiSgE1iPoNdJTIANh2/jo8XWgtQ6BbDN25J0QuSTyMNcgthe7KwI8Y2Fy4gbEjBlFHjIblhSy0HpxdoPP3wizgBweXYUoB0L8GTJzw4NEp8hMQe1jEPsKdlTvQ8SIkDyTKRK3rIz4eCeEIpqnweuUYn74DWljSVG75Fh8tydsR0G9LhGFNJPUdYIR33KpWfAvYdeCy2B7EDoWbK5JcEbhuqszEWSVvKpXajpzwQCRN+SYjA1qXk6Vgeq0/4WtOmRaFouyIvZHEJ8jzVPgE07qqS+SKcW24tRWzYvIJPwh7jtAnaZA1EmxaCy2cPBaigt2zGGhyEBB1ZyIr5IVi6Q3xPQS9KvfPPyDmuECVTYoJ4bsjIWMsChHs2p83U/sI6VqUdgxh0VNtNjPZFQoTblJjUl8EkJXZ1LPUPtv1WZ/ajh1uNRlbyX1xQ+oSKe4obUDmoCtXKl8ATsWxNChfrbEiVzFvFJqQK8ITGZDkXAUsWdEv1UK0WMq2OK7a43Mr+kqJKkW8U+oox3VMCHW6bSkVB0yxMwVrLwzusHSO23PodsvmWthz2m+QafdQRXXyUAGvMvMtmArTVdIvY1SpVbpLchwlXKSmsmimqFFEGvYodTEUL/57pmyoMnY6Etp/gSlDXx9+QvjpP5Mesr396ReX/kHFpg0/DtaMDucpYQd0WxqmTOl8gbbkg0zXZfCi//H4/G/m/EvcvMv9vkuwACh7obKLanwzQAAAABJRU5ErkJggg=="></a>
    			<style type="text/css">.deltalogo{position:relative;display:block;cursor:pointer;width:117px;height:24px}.deltalogo img:first-child,.deltalogo img:nth-child(2){transition:all .4s;-ms-transform:rotate(0);-moz-transform:rotate(0);-webkit-transform:rotate(0);-o-transform:rotate(0);transform:rotate(0);opacity:0;position:absolute;right:0;top:-1px}.deltalogo img:nth-child(2),.deltalogo:hover img:first-child{opacity:1}.deltalogo:hover img:first-child,.deltalogo:hover img:nth-child(2){-ms-transform:rotate(360deg);-moz-transform:rotate(360deg);-webkit-transform:rotate(360deg);-o-transform:rotate(360deg);transform:rotate(360deg);right:87px}.deltalogo:hover img:nth-child(2){opacity:0}.deltalogo img:last-child{transition:all .1s;position:absolute;right:0;top:6px;opacity:0}.deltalogo:hover img:last-child{transition:all 1s;opacity:1}</style>
    </div>

</body>
@yield('custom_scripts')
</html>

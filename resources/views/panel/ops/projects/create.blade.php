@extends('layouts.panel_master')

@section('breadcrumb')

  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-12">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/panel">Anasayfa</a></li>
            <li class="breadcrumb-item"><a href="{{route('projects.index')}}">Projeler</a></li>
            <li class="breadcrumb-item active">Proje Ekle</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->
@endsection

@section('content')



  <!-- Main content -->
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- /.col -->
        <div class="col-12">
          <div class="card">
            <div class="card-body">

              {!! Form::open(['route'=>'projects.store', 'files'=>'true', 'data-parsley-validate'=>'', 'data-parsley-errors-container'=>'.parsley_error']) !!}
                <div class="row">
                  <div class="col-12">
                    <div class="card card-info">
                      <div class="card-header">
                        <h3 class="card-title">Proje Bilgileri</h3>
                      </div>
                      <!-- /.card-header -->
                      <div class="card-body">

                        <div class="form-row">
                          <div class="col-12 col-md-12">
                            {{ Form::label('durum', 'Proje Durumu', ['class' => 'col-form-label']) }}
                            <div class="input-group mb-3">
                              {{ Form::select('durum', [0=>'Biten Proje',1=>'Devam Eden Proje'],null, ['class'=>'form-control', 'required'=>'','autofocus'=>'', 'data-parsley-error-message'=>'Adı zorunlu alan.']) }}
                              <div class="input-group-append">
                                  <span class="fas fa-vector-square fa-fw input-group-text"></span>
                              </div>
                            </div>
                          </div>

                          <div class="col-12 col-md-4">
                            {{ Form::label('adi', 'Adı', ['class' => 'col-form-label']) }}
                            <div class="input-group mb-3">
                              {{ Form::text('adi', null, ['class'=>'form-control', 'required'=>'','autofocus'=>'', 'data-parsley-error-message'=>'Adı zorunlu alan.']) }}
                              <div class="input-group-append">
                                  <span class="fas fa-vector-square fa-fw input-group-text"></span>
                              </div>
                            </div>
                          </div>

                          <div class="col-12 col-md-4">
                            {{ Form::label('baslangic_tarihi', 'Başlangıç Tarihi', ['class' => 'col-form-label']) }}
                            <div class="input-group mb-3">
                              {{ Form::date('baslangic_tarihi', null, ['class'=>'form-control']) }}
                              <div class="input-group-append">
                                  <span class="fas fa-vector-square fa-fw input-group-text"></span>
                              </div>
                            </div>
                          </div>

                          <div class="col-12 col-md-4">
                            {{ Form::label('ongorulen_bitis_tarihi', 'Öngörülen Bitiş Tarihi', ['class' => 'col-form-label']) }}
                            <div class="input-group mb-3">
                              {{ Form::date('ongorulen_bitis_tarihi', null, ['class'=>'form-control']) }}
                              <div class="input-group-append">
                                  <span class="fas fa-vector-square fa-fw input-group-text"></span>
                              </div>
                            </div>
                          </div>

                          <div class="col-12 col-md-3">
                            {{ Form::label('satisa_acik_daire', 'Satışa Açık Daire', ['class' => 'col-form-label']) }}
                            <div class="input-group mb-3">
                              {{ Form::number('satisa_acik_daire', null, ['class'=>'form-control']) }}
                              <div class="input-group-append">
                                  <span class="fas fa-vector-square fa-fw input-group-text"></span>
                              </div>
                            </div>
                          </div>

                          <div class="col-12 col-md-3">
                            {{ Form::label('arsa_alani', 'Arsa Alanı', ['class' => 'col-form-label']) }}
                            <div class="input-group mb-3">
                              {{ Form::number('arsa_alani', null, ['class'=>'form-control']) }}
                              <div class="input-group-append">
                                  <span class="fas fa-vector-square fa-fw input-group-text"></span>
                              </div>
                            </div>
                          </div>

                          <div class="col-12 col-md-3">
                            {{ Form::label('insaat_alani', 'İnşaat Alanı', ['class' => 'col-form-label']) }}
                            <div class="input-group mb-3">
                              {{ Form::number('insaat_alani', null, ['class'=>'form-control']) }}
                              <div class="input-group-append">
                                  <span class="fas fa-vector-square fa-fw input-group-text"></span>
                              </div>
                            </div>
                          </div>

                          <div class="col-12 col-md-3">
                            {{ Form::label('yesil_alan', 'Yeşil Alan', ['class' => 'col-form-label']) }}
                            <div class="input-group mb-3">
                              {{ Form::number('yesil_alan', null, ['class'=>'form-control']) }}
                              <div class="input-group-append">
                                  <span class="fas fa-vector-square fa-fw input-group-text"></span>
                              </div>
                            </div>
                          </div>

                          <div class="col-12 col-md-3">
                            {{ Form::label('toplam_daire', 'Toplam Daire', ['class' => 'col-form-label']) }}
                            <div class="input-group mb-3">
                              {{ Form::number('toplam_daire', null, ['class'=>'form-control']) }}
                              <div class="input-group-append">
                                  <span class="fas fa-vector-square fa-fw input-group-text"></span>
                              </div>
                            </div>
                          </div>

                          <div class="col-12 col-md-9">
                            {{ Form::label('slogan', 'Slogan', ['class' => 'col-form-label']) }}
                            <div class="input-group mb-3">
                              {{ Form::text('slogan', null, ['class'=>'form-control']) }}
                              <div class="input-group-append">
                                  <span class="fas fa-vector-square fa-fw input-group-text"></span>
                              </div>
                            </div>
                          </div>

                          <div class="col-12 col-md-4">
                            {{ Form::label('adres', 'Adres', ['class' => 'col-form-label']) }}
                            <div class="input-group mb-3">
                              {{ Form::text('adres', null, ['class'=>'form-control']) }}
                              <div class="input-group-append">
                                  <span class="fas fa-vector-square fa-fw input-group-text"></span>
                              </div>
                            </div>
                          </div>

                          <div class="col-12 col-md-4">
                            {{ Form::label('telefon', 'Telefon', ['class' => 'col-form-label']) }}
                            <div class="input-group mb-3">
                              {{ Form::text('telefon', null, ['class'=>'form-control']) }}
                              <div class="input-group-append">
                                  <span class="fas fa-vector-square fa-fw input-group-text"></span>
                              </div>
                            </div>
                          </div>

                          <div class="col-12 col-md-4">
                            {{ Form::label('koordinatlar', 'Koordinatlar', ['class' => 'col-form-label']) }}
                            <div class="input-group mb-3">
                              {{ Form::text('koordinatlar', null, ['class'=>'form-control']) }}
                              <div class="input-group-append">
                                  <span class="fas fa-vector-square fa-fw input-group-text"></span>
                              </div>
                            </div>
                          </div>

                          <div class="col-12 col-lg-4 mt-3">
                            {{ Form::label('proje_gorsel','Ana Proje Görseli Seçin (Slider) (1920x900)', ['class'=>'col-form-label']) }}
                            <div>
                              <div class="input-group">
                                <div class="custom-file">
                                  {{ Form::file('proje_gorsel',['class'=>'custom-file-input'])}}
                                  {{ Form::label('proje_gorsel','Görsel Seçin', ['class'=>'custom-file-label']) }}
                                </div>
                                <div class="input-group-append">
                                  <span class="input-group-text"><i class="fas fa-file fa-fw"></i></span>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="col-12 col-lg-4 mt-3">
                            {{ Form::label('kat_plani_gorsel','Vaziyet Planı Görseli Seçin (479x654)', ['class'=>'col-form-label']) }}
                            <div>
                              <div class="input-group">
                                <div class="custom-file">
                                  {{ Form::file('kat_plani_gorsel',['class'=>'custom-file-input'])}}
                                  {{ Form::label('kat_plani_gorsel','Görsel Seçin', ['class'=>'custom-file-label']) }}
                                </div>
                                <div class="input-group-append">
                                  <span class="input-group-text"><i class="fas fa-file fa-fw"></i></span>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="col-12 col-lg-4 mt-3">
                            {{ Form::label('logo','Proje Logosu Seçin (230x115)', ['class'=>'col-form-label']) }}
                            <div>
                              <div class="input-group">
                                <div class="custom-file">
                                  {{ Form::file('logo',['class'=>'custom-file-input'])}}
                                  {{ Form::label('logo','Logo Seçin', ['class'=>'custom-file-label']) }}
                                </div>
                                <div class="input-group-append">
                                  <span class="input-group-text"><i class="fas fa-file fa-fw"></i></span>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="col-12 mt-3">
                            {{ Form::label('proje_gorselleri','Proje Görselleri Seçin', ['class'=>'col-form-label']) }}
                            <div>
                              <div class="input-group">
                                <div class="custom-file">
                                  {{ Form::file('proje_gorselleri[]',['class'=>'custom-file-input', 'multiple'=>''])}}
                                  {{ Form::label('proje_gorselleri','Görselleri Seçin', ['class'=>'custom-file-label']) }}
                                </div>
                                <div class="input-group-append">
                                  <span class="input-group-text"><i class="fas fa-file fa-fw"></i></span>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="col-12 mt-3">
                            {{ Form::label('genel_ozellikler', 'Genel Özellikler', ['class' => 'col-form-label']) }}
                            <div class="mb-3">
                              {{ Form::text('genel_ozellikler', null, ['class'=>'form-control tinymce'])}}
                            </div>
                          </div>

                          <div class="col-12 mt-3">
                            <table class="table table-sm table-bordered">
                              <thead>
                                <tr>
                                  <th colspan="4">Kat Planları</th>
                                  <th style="width:45px; min-width:45px; max-width:45px;"><button class="btn btn-sm btn-primary add_floor_plan"><i class="fas fa-plus-circle"></i></button></th>
                                </tr>
                              </thead>
                              <tbody>

                              </tbody>
                            </table>
                          </div>

                        </div>
                      </div>
                    </div>
                  </div>
                </div>


                <div class="row">
                  <div class="col-10">

                  </div>
                  <!-- /.col -->
                  <div class="col-2">
                    <button type="submit" class="btn btn-primary btn-block">Kaydet</button>
                  </div>
                  <!-- /.col -->
                </div>

              {!! Form::close() !!}

            </div>
          </div>
        </div>
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content -->
  <script>
    $(document).ready(function() {

    })
  </script>
@endsection

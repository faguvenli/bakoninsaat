@extends('layouts.panel_master')

@section('breadcrumb')

  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-12">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Anasayfa</a></li>
            <li class="breadcrumb-item active">Profil</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->
@endsection

@section('content')




  <!-- Main content -->
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- /.col -->
        <div class="col-12">
          <div class="card">
            <div class="card-body">

              <div class="profile-header-container mb-5">
                <div class="profile-header-img">
                  <img class="rounded-circle" width="100" height="100" src="/images/{{ $user->avatar }}" />
                  <!-- badge -->
                </div>
              </div>

              {!! Form::model($user, ["route"=>["profile.update",$user->id],"method"=>"post","files"=>true, "data-parsley-validate"=>"", "data-parsley-errors-container"=>".parsley_error"]) !!}

                <div class="row">
                  <div class="col-12 col-sm-6">
                    <div class="form-row">
                      {{ Form::label('name','Ad Soyad') }}
                      <div class="input-group mb-3">
                        {{ Form::text('name',null,['class'=>'form-control','required'=>'','data-parsley-error-message'=>'Ad Soyad zorunlu alan.'])}}
                        <div class="input-group-append">
                            <span class="fas fa-id-card fa-fw input-group-text"></span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-12 col-sm-6">
                    <div class="form-row">
                      {{ Form::label('email','E-Posta') }}
                      <div class="input-group mb-3">
                        {{ Form::text('email',null,['class'=>'form-control','required'=>'','data-parsley-error-message'=>'E-posta zorunlu alan.'])}}
                        <div class="input-group-append">
                            <span class="fas fa-at fa-fw input-group-text"></span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-12 col-sm-6">
                    <div class="form-row">
                      {{ Form::label('avatar','Profil Fotoğrafı') }}
                      <div class="col-12">
                        {{ Form::file('avatar',null,['class'=>'form-control'])}}
                      </div>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-12 col-sm-6">
                    <div class="form-row">
                      {{ Form::label('password','Parola') }}
                      <div class="input-group mb-3">
                        {{ Form::password('password',['class'=>'form-control'])}}
                        <div class="input-group-append">
                            <span class="fas fa-lock fa-fw input-group-text"></span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-12 col-sm-6">
                    <div class="form-row">
                      {{ Form::label('password_confirmation','Parola (Tekrar)') }}
                      <div class="input-group mb-3">
                        {{ Form::password('password_confirmation',['class'=>'form-control'])}}
                        <div class="input-group-append">
                            <span class="fas fa-lock fa-fw input-group-text"></span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="form-row">
                  <div class="col-10">

                  </div>
                  <!-- /.col -->

                  <div class="col-2">
                    {{ Form::submit('Güncelle',['class'=>'btn btn-primary btn-block'])}}
                  </div>
                  <!-- /.col -->
                </div>

              {!! Form::close() !!}

            </div>
          </div>
        </div>
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content -->
  <script>
    $(document).ready(function() {

    })
  </script>
@endsection

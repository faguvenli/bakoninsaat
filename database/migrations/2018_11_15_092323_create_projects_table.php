<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('durum')->default(0);
            $table->string('adi');
            $table->string('logo')->nullable();
            $table->date('baslangic_tarihi')->nullable();
            $table->date('ongorulen_bitis_tarihi')->nullable();
            $table->string('satisa_acik_daire')->nullable();
            $table->string('arsa_alani')->nullable();
            $table->string('insaat_alani')->nullable();
            $table->string('yesil_alan')->nullable();
            $table->string('toplam_daire')->nullable();
            $table->string('proje_gorsel')->nullable();
            $table->string('kat_plani_gorsel')->nullable();
            $table->string('slogan')->nullable();
            $table->text('genel_ozellikler')->nullable();
            $table->string('adres')->nullable();
            $table->string('telefon')->nullable();
            $table->string('koordinatlar')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
